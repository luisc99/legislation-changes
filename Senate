Senate of VillageCraft Official Guidelines:
All Senate participants must be Members of Parliament.

A
The Senate:
1. Roles
i. The Senate of VillageCraft is made up of Senators. The Senate discusses, debates, and votes on proposing legislation to send to the Parliament of VillageCraft for binding Official Parliament Votes. The Senate is a body of the Parliament of VillageCraft.

ii. The Senate may pass Senate internal rules, without needing a Parliament Vote, which are binding on the Senate and remain in force between Senate Sessions. Internal rules cannot conflict with this document.

iii. The Senate can also investigate Staff Members of wrongdoing if there is reasonable suspicion a rule was broken, and without needing a Parliament Vote. The Senate does not need to vote to begin an investigation. They can optionally vote to begin one, in which case the Staff must comply with the investigation. The Senate can vote to formally deliver their findings and/or recommendations to the Staff. Senators may not participate in Senate Votes that involve investigations into themselves. Privileged or confidential information given to the Senate must remain private within the Senate, except in the case where the Senate votes to deliver their findings to the Staff, and the information is relevant.

2. Exclusions
i. The Senate cannot not pass binding legislation on VC without Official Parliament Votes, except for internal rules which govern the Senate themselves, starting investigations into Staff Members, filling seat vacancies, or increasing the total seats to 7.

ii. The Senate does not pass or propose legislation regulating the internal rules of the VillageCraft Staff, appoint or remove Staff Members, or participate in Staff Votes.

B
Senate Sessions:

1. Sessions Last 4 Months
i. Senate Sessions last 4 calendar months. 

ii. Sessions end and begin at Noon on the first Wednesday of the respective 4th month after the Session began. (Toronto-Montreal-New York Noon is used.)

2. Ends of Sessions
i. Once a Session ends, in-progress proceedings are stopped and removed, and all current Senators are removed. The next Senate Session starts anew. Senate internal rules remain in force.

3. New Sessions
i. Immediately once a Senate Session ends, a new one begins. 24 hours to 10 days prior to this moment, the CM had the opportunity to accept a seat in the new Session, as seen in “D.2.”. At this moment, all remaining seats are filled by Sortition, as seen in “C.5.”. 

ii. The new Senate may begin proceedings once 5 Senators are confirmed.

C
Senate Seats:

1. New Sessions Have 5 Seats
i. The Senate shall have 5 total seats at the start of a new Senate Session.

2. Increase to 7 Seats
i. The Senate may vote to increase the total seats to 7 for the remainder of the current Session. If this vote fails, it may not occur again for 7 days.

ii. 7 is the maximum possible number of seats.

3. Vacant Seats During a Session
i. The Senate may vote to order vacant seats be filled by a Selection Event from the Current Sortition List at any time. If this vote fails, it may not occur again for 7 days. 

ii. The Parliament of VillageCraft may use a Simple Parliament Vote to order vacant seats be filled by a Selection Event from the Current Sortition List at any time. If this vote fails, it may not occur again for 7 days.

iii. The Speaker of the Senate may bypass voting and choose to order vacant seats be filled by a Selection Event from the Current Sortition List at any time.

iv. Orders to fill vacant seats may not occur within the last 72 hours of a Senate Session.

4. Removal
i. Removed Senators cannot return to the Senate until the next Session. Senators may remove themselves at any time by resigning. The resignation must be clear and unambiguous. If the Community Minister is removed from the Senate due to ceasing to be Community Minister, but they remain on the Sortition list, they remain eligible to be selected to join the same Senate session via Sortition.

ii. The Parliament of VillageCraft may use a Simple Parliament Vote to remove one or more Senators at any time. If a removal vote fails, another vote to remove the Senator(s) in question may not occur again for 7 days. 

iii. All Senators are removed at the end of a Senate Session.

5. Sortition
i. Each Senate Session has a Current Sortition List attached to it, from which Senators are selected in Selection Events. Senate Sortition uses the Sortition Method, which produces a new random Selection List every day at 00:01 UTC. VC’s full publicly auditable Sortition Method and daily Selection Lists and are viewable any time at [link].

ii. Any MP may self-nominate their name to the Upcoming Sortition List for possible selection in the next Senate. 

iii. 24 hours before the start of a new Senate Session, the "Upcoming Sortition List" becomes the new "Current Sortition List", and is closed to new names being added. During an ongoing Session, a new "Upcoming Sortition List" is opened for self-nominations to the next Senate Session.

iv. The Selection List dated for the first day of a new Senate Session will be used in that Selection Event to fill all seats for the incoming Senate, except for one if it is due to be held by the Community Minister (as described in “D.2.”).

v. When an order occurs for Senate seats to be filled in “C.3.”, the next scheduled daily Selection List will be used to determine the seat(s) for that Selection Event.

6. Soft Limit of 3 Staff Members on the Senate
i. If there are 3 or more members of Staff on the Senate during a Selection Event, Staff names shall be skipped during selection. 

ii. If a Senator becomes a member of Staff while in the Senate, they retain their seat, however they will be counted towards the total number of Staff in all future Selection Events during that session. This may result in the Senate consisting of more than 3 Staff members. If a Senator ceases to be a member of Staff, they will no longer be counted towards the total number of staff in future Selection Events.


D
Senators:

1. Must Be MPs
i. All Senators must be VillageCraft Members of Parliament.

2. Community Minister Senate Seat
i. Prior to a new Senate Session, before Sortition, the Community Minister chooses whether to sit as a Senator for the upcoming Session. 

ii. The CM can indicate their choice up to 10 days before a new Senate Session begins, and must make their decision before the Upcoming Sortition List is closed (24 hours before the start of the new Session).

iii. If the CM does not choose, declines the seat, fails the Competency Test, resigns from the Senate, or ceases being CM, the choice is not offered again until the start of the next Senate Session. 

iv. If the CM is a Senator and ceases to be the CM, that individual is removed from the Senate, creating a vacant seat.

3. All Other Senators Selected by Sortition
i. After the CM chooses whether to be a Senator, all remaining Senate seats are selected by Sortition from the Current Sortition List, as seen in “C.5.”, and are confirmed upon successful completion of the Competency Test.


4. Competency Test and Confirmation
i. Anyone offered a Senate seat must pass the Competency Test to be confirmed to the Senate. The test must be completed within 5 days and 2 attempts.

ii. The Test includes simple math, assurance the person is active, able to serve, and understands the rules and role of the Senate and Senators. The test will be written and administered by the VC Staff.

E
Speaker of the Senate:

1. Role of the Speaker
i. The Speaker runs the proceedings of the Senate, and conducts Senate Votes. 

ii. The Speaker must run proceedings in a timely manner without significant delay.

2. Elected by the Senate
i. The Speaker of the Senate is a fellow Senator who is majority elected by the Senate. Election votes may occur infinitely until a Speaker is chosen. 

3. Removal
i. The Speaker may step down at any time. The Speaker may be removed from their role at any time by a majority Senate Vote. If a removal vote fails, another may not occur for 7 days.

ii. If the Speaker breaches procedure and fails to hold a removal vote, they are automatically removed at the request of any Senator.

4. There Must Be a Speaker
i. If there is no Speaker of the Senate at any time, the Senate must elect one. No procedure or votes may occur until there is a Speaker. 

F
Senate Procedure and Votes:

1. Discussion
i. Senators shall discuss ideas to bring to a Senate Vote. The Speaker shall moderate the discussion to keep it constructive if needed. 

ii. If an idea is decided upon, Senators may write a Draft of the legislation to be debated. "Legislation" can include Senate internal rules, proposals for Official Parliament Votes, Staff Member investigations, removal of the Speaker, filling seat vacancies, increasing to 7 seats, or anything else within the scope of the Senate.

2. Draft
i. Based on Senate discussions, Senators shall write Draft legislation and request a Debate on it. 

ii. The Speaker shall make sure the Draft is acceptable within the Role of the Senate, and if it is, hold to a Debate on it.

3. Debate Draft Legislation
i. The Speaker shall hold a Debate on submitted Draft legislation. The Speaker shall respect the chronological order in which Drafts were submitted, including Speaker removal vote requests, holding Debates in the order of their submission. 

ii. Debates shall be whether to improve upon the current wording, scrap the idea, or keep the current wording.

iii. Senators shall take these ideas and consider submitting improved Drafts, or Nominating Draft Legislation for a Senate Vote.

4. Nominate Draft Legislation for Senate Vote
i. Based upon the proceedings of a Debate, Drafts may be nominated by 2 or more Senators to proceed to a Senate Vote. 

ii. The Speaker shall respect the chronological order in which nominations and Nominated Draft Legislation is submitted, holding Senate Votes in the order of their submission. 

5. Senate Votes
i. The Speaker shall hold a Senate Vote for Nominated Draft Legislation, which must include a clear question, clear answers, and the ability to abstain. Votes pass with a majority. The Speaker shall run the Senate Vote for 5 days, or until all Senators have voted, at the Speaker's discretion.

ii. All Senate votes are recorded and made public.

iii. If the Senate Vote is defeated, it cannot be voted on again for 7 days if the text is not meaningfully changed.

6. Successful Senate Votes
i. If a Senate Vote succeeds and creates, amends, or revokes an internal Senate rule, it is effective immediately and becomes Senate internal legislation.

ii. If a Senate Vote succeeds in increasing Senate seats to 7, filling vacant seats, removing the Speaker, or commencing an investigation of a Staff Member, it is effective immediately.

iii. If a Senate Vote succeeds and proposes an Official Parliament Vote, it moves on to the Consent of the Staff stage. A Senator shall deliver the final text of the proposal to the Staff.

7. Consent of the Staff (for Official Parliament Vote Proposals)
i. The final text of the Official Parliament Vote Proposal must be approved by 3 Staff members based solely on feasibility, viability, and ability to implement the proposal if successful. This shall not be based on personal approval or disapproval.

ii. In the case where a System Admin must be involved in the implementation, consent of at least 1 must be gained. A System Admin is counted as one of the 3 required Staff in “F.7.i.”.

iii. If there is genuine reasonable disapproval from a Staff member, the proposal may be put to Staff vote. If it fails, and/or if needed, Staff may request a rewrite of the Official Parliament Vote Proposal before they give consent, in which case the process returns to Stage "F.3.i."

iv. If Consent of the Staff has been given, the Staff shall inform the Senate. The initiative can then proceed to an Official Parliament Vote, which shall be initiated by the Speaker of the Senate, the Community Minister, or the Staff, at the request of any Senator.


All Senate participants must be Members of Parliament.
